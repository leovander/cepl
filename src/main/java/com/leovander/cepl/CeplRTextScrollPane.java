package com.leovander.cepl;

import org.fife.ui.rtextarea.RTextScrollPane;

import java.awt.*;

/**
 * Created by israeltorres on 12/12/16.
 */
class CeplRTextScrollPane extends RTextScrollPane {

    CeplRTextScrollPane(CeplRSyntaxTextArea evalInputTextArea) {
        super(evalInputTextArea);

        getGutter().setBackground(Color.decode("#D4D4D4"));
        setBackground(Color.decode("#FFFFFF"));
        setFoldIndicatorEnabled(false);
    }
}