package com.leovander.cepl;

import org.fife.ui.rsyntaxtextarea.RSyntaxDocument;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * Created by israeltorres on 12/12/16.
 */
class CeplRSyntaxTextArea extends RSyntaxTextArea {
    CeplRSyntaxTextArea(int i, int i1) {
        super(i, i1);
        setDefaults();
    }

    CeplRSyntaxTextArea(int i, int i1, boolean documentListener) {
        super(i, i1);
        setDefaults();

        if(documentListener == true) {
            getDocument().addDocumentListener(new CeplDocumentListener());
        }
    }

    private void setDefaults() {
        setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);
        setCodeFoldingEnabled(true);
        setLineWrap(true);
        setWrapStyleWord(true);
        setMarkOccurrences(true);
    }

    class CeplDocumentListener implements DocumentListener {
        @Override
        public void insertUpdate(DocumentEvent e) {
            System.out.println("Inserted: " + e.getOffset() + ", " + e.getLength());

            try {
                System.out.println("Inserted: " + e.getDocument().getText(e.getOffset(), e.getLength()));
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            System.out.println("Removed content");
            // TODO: Hook remove/insert method to the cache as well as store previous version (Tap into RSyntaxArea?)
        }

        @Override
        public void changedUpdate(DocumentEvent e) { }
    }
}