package com.leovander.cepl;

import javax.swing.*;

/**
 * Created by israeltorres on 12/12/16.
 */
public class Cepl {
    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new CeplUI().setVisible(true);
            }
        });
    }
}