package com.leovander.cepl;

import net.miginfocom.swing.MigLayout;
import org.mozilla.javascript.tools.debugger.Dim;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;

/**
 * Created by israeltorres on 12/12/16.
 */
public class CeplUI extends JFrame implements ComponentListener, WindowStateListener{
    private final static int FRAME_WIDTH = 1280;
    private final static int FRAME_HEIGHT = 720;

    private JSplitPane mainSplitPane;
    private CeplRTextScrollPane evalLeftScrollPane;
    private CeplRTextScrollPane evalRightScrollPane;
    private CeplRSyntaxTextArea evalInputTextArea;
    private CeplRSyntaxTextArea evalOutputTextArea;

    private int previousFrameWidth;

    CeplUI() {
        new JFrame();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLayout(new MigLayout("ins 0, wrap 1, fill", "grow"));
        setMinimumSize(new Dimension(FRAME_WIDTH,FRAME_HEIGHT));
        setLocationRelativeTo(null);
        setTitle("CEPL");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        initMainPanels();

        previousFrameWidth = getWidth();
        addComponentListener(this);
        addWindowStateListener(this);
    }

    private void initMainPanels() {
        mainSplitPane = new JSplitPane();
        mainSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
        mainSplitPane.setContinuousLayout(true);
        mainSplitPane.setOneTouchExpandable(true);

        initEvalPanels();

        mainSplitPane.setLeftComponent(evalLeftScrollPane);
        mainSplitPane.setRightComponent(evalRightScrollPane);

        updateSplitPaneDivider();

        mainSplitPane.setResizeWeight(0.5);

        add(mainSplitPane, "grow");
        pack();
    }

    private void initEvalPanels() {
        initTextAreas();

        evalLeftScrollPane = new CeplRTextScrollPane(evalInputTextArea);
        evalRightScrollPane = new CeplRTextScrollPane(evalOutputTextArea);
    }

    private void initTextAreas() {
        evalInputTextArea = new CeplRSyntaxTextArea(10,40, true);

        evalOutputTextArea = new CeplRSyntaxTextArea(10,40);
        evalOutputTextArea.setEditable(false);
    }

    @Override
    public void componentResized(ComponentEvent e) {
        updateFrameWidth();
    }

    private boolean isNewFrameWidth(int newFrameWidth) {
        return newFrameWidth != previousFrameWidth;
    }

    private void updateFrameWidth() {
        if(isNewFrameWidth(getWidth())) {
            updateSplitPaneDivider();
        }

        previousFrameWidth = getWidth();
    }

    private void updateSplitPaneDivider() {
        int defaultDividerLocation = (int) Math.floor((getWidth() - mainSplitPane.getDividerSize()) * 0.66);
        mainSplitPane.setDividerLocation(defaultDividerLocation);

        int minimumComponentWidth = (int) Math.floor((getWidth() - mainSplitPane.getDividerSize()) * 0.33);
        Dimension minimumComponentDimension = new Dimension(minimumComponentWidth, mainSplitPane.getHeight());
        mainSplitPane.getRightComponent().setMinimumSize(minimumComponentDimension);
        mainSplitPane.getLeftComponent().setMinimumSize(minimumComponentDimension);
    }

    @Override
    public void componentMoved(ComponentEvent e) {}

    @Override
    public void componentShown(ComponentEvent e) {}

    @Override
    public void componentHidden(ComponentEvent e) {}

    @Override
    public void windowStateChanged(WindowEvent e) {
        if(e.getNewState() == 0){
            // Resets window from either minimized state or from maximized state
            updateFrameWidth();
        } else if(e.getNewState() == 1 || e.getNewState() == 7){
            // Hides application to task bar from normal or maximized state
            // TODO: Save contents to disk
        } else  if(e.getNewState() == 6){
            // Application maximized
        }
    }
}
