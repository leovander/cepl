# Connect-Eval-Print-Loop (CEPL)
CEPL is a play on the commonly known [read-eval-print-loop](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop), that allows the user to input commands and have the contents evaluated and printed out to the user.

## Build
There is an included pom file that you can build the project with
[maven](https://maven.apache.org/).

## Running
By default your commands can be run by using F5.

## Features
Because most functionality needed from a channel is a JavaScript context that matches that of what is running in Mirth Connect, by default CEPL only provides a UI wrapper around [Rhino](https://developer.mozilla.org/en-US/docs/Mozilla/Projects/Rhino), the same scripting engine that is used within [NextGen Connect](https://www.nextgen.com/products-and-services/integration-engine) to run JavaScript.